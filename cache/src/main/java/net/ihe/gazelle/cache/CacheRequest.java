package net.ihe.gazelle.cache;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.HashMap;
import java.util.Map;

@Named("cacheRequest")
@RequestScoped
public class CacheRequest extends CacheAbstract {
    private Map<String, Object> cache;

    @Override
    public Object getValue(String key) {
        return cache.get(key);
    }

    @Override
    public void setValue(String key, Object value) {
        cache.put(key, value);
    }

    @Override
    public void removeValue(String key) {
        cache.remove(key);
    }

    @PostConstruct
    public void init() {
        cache = new HashMap<String, Object>();
    }

    @PreDestroy
    public void clear() {
        if (cache != null) {
            cache.clear();
        }
    }
}
