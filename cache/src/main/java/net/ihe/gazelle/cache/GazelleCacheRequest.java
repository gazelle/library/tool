package net.ihe.gazelle.cache;

public interface GazelleCacheRequest<T> {
    T get(String objectId);
}
