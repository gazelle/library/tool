package net.ihe.gazelle.cache.file;

import java.io.OutputStream;

/**
 * Render files depending of a value. Look at QRCodeRenderer for example.
 */
public interface FileCacheRenderer {

	void render(OutputStream out, String value) throws Exception;

	String getContentType();

}
