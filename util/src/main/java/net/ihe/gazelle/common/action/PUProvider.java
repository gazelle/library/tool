package net.ihe.gazelle.common.action;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

@Stateless(name = "PUProvider")
public class PUProvider {

    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @Produces
    public EntityManagerFactory getFactory() {return entityManagerFactory;}

}