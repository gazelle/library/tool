package net.ihe.gazelle.common.messaging;

import javax.enterprise.inject.Instance;
import javax.enterprise.inject.spi.CDI;

public class MessagingService {

	public static void publishMessage(Object message) {
		Instance<MessagingProvider> providers = CDI.current().select(MessagingProvider.class);
		for (MessagingProvider messagingProvider : providers) {
			messagingProvider.receiveMessage(message);
		}
	}

	private MessagingService() {
		super();
	}

}
