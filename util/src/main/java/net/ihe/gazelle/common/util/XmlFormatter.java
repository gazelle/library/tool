package net.ihe.gazelle.common.util;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

/**
 * @author abderrazek boufahja
 */
public class XmlFormatter {

    private XmlFormatter() {

    }

    public static String format(String xmlString) throws DocumentException, IOException {
        ByteArrayInputStream is = new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8));
        SAXReader reader = new SAXReader();
        Document document = reader.read(is);
        return formatDocument(document);
    }

    public static String formatPath(String xmlpath) throws IOException, DocumentException {
        SAXReader reader = new SAXReader();
        Document document = reader.read(xmlpath);
        return formatDocument(document);
    }

    public static String formatDocument(Document document) throws IOException {
        OutputFormat format = OutputFormat.createPrettyPrint();
        StringWriter w = new StringWriter();
        XMLWriter writer = new XMLWriter(w, format);
        writer.write(document);
        writer.close();
        return w.getBuffer().toString();
    }

    public static String prettyFormat(String input, int indent) {
        try {
            Source xmlInput = new StreamSource(new StringReader(input));
            StringWriter stringWriter = new StringWriter();
            StreamResult xmlOutput = new StreamResult(stringWriter);
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", String.valueOf(indent));
            transformer.transform(xmlInput, xmlOutput);
            return xmlOutput.getWriter().toString();
        } catch (Exception e) {
            return input;
        }
    }
}
