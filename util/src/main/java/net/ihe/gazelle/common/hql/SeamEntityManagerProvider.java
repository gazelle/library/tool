package net.ihe.gazelle.common.hql;

import net.ihe.gazelle.hql.providers.EntityManagerProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.ContextNotActiveException;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Named
@Deprecated
//TODO Use specific implementation instead or put implementation in gazelle-hql ?
public class SeamEntityManagerProvider implements EntityManagerProvider {

    private static final Logger log = LoggerFactory.getLogger(SeamEntityManagerProvider.class);
    private static final int LIGHT_WEIGHT = -100;
    private static final int HEAVY_WEIGHT = 100;

    @PersistenceContext
    private EntityManager entityManager;

    @Inject
    private BeanManager beanManager;

    @Override
    public EntityManager provideEntityManager() {
        if (entityManager != null) {
            return entityManager;
        } else {
            log.error("No entity Manager Available !");
            return null;
        }
    }

    @Override
    public Integer getWeight() {
        log.debug("BeanManager is null : {}", beanManager != null);
        try {
            if (beanManager != null && beanManager.getContext(ApplicationScoped.class).isActive()) {
                return LIGHT_WEIGHT;
            } else {
                return HEAVY_WEIGHT;
            }
        } catch (final ContextNotActiveException | NullPointerException e) {
            log.error("Error checking context :", e);
            return HEAVY_WEIGHT;
        }
    }

    @Override
    public int compareTo(EntityManagerProvider o) {
        return getWeight().compareTo(o.getWeight());
    }
}
