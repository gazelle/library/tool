package net.ihe.gazelle.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.*;

/**
 * @author abderrazek boufahja
 */
public class ZipCreation {

    private static Logger log = LoggerFactory.getLogger(ZipCreation.class);

    private static final int BUFFER = 2048;

    private ZipCreation() {
    }

    @SuppressWarnings("unchecked")
    public static void Unzip(String pathzip) throws IOException {
        BufferedOutputStream dest = null;
        BufferedInputStream is = null;
        ZipEntry entry = null;
        ZipFile zipfile = null;
        try {
            zipfile = new ZipFile(pathzip);
            Enumeration<?> e = zipfile.entries();
            while (e.hasMoreElements()) {
                entry = (ZipEntry) e.nextElement();
                is = new BufferedInputStream(zipfile.getInputStream(entry));
                int count;
                byte data[] = new byte[BUFFER];
                FileOutputStream fos = new FileOutputStream(entry.getName());
                dest = new BufferedOutputStream(fos, BUFFER);
                while ((count = is.read(data, 0, BUFFER)) != -1) {
                    dest.write(data, 0, count);
                }
                dest.flush();
                dest.close();
                is.close();
                zipfile.close();
            }
        } catch (Exception e) {
            log.error("", e);
        } finally {
            if (zipfile != null) {
                zipfile.close();
            }
        }
    }

    public static void files2zip(String pathzip, List<String> list_files) {
        try {
            BufferedInputStream origin = null;
            FileOutputStream dest = new FileOutputStream(pathzip);
            CheckedOutputStream checksum = new CheckedOutputStream(dest, new Adler32());
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(checksum));
            byte data[] = new byte[BUFFER];

            for (int i = 0; i < list_files.size(); i++) {

                FileInputStream fi = new FileInputStream(list_files.get(i));
                origin = new BufferedInputStream(fi, BUFFER);
                ZipEntry entry = new ZipEntry(list_files.get(i));
                out.putNextEntry(entry);
                int count;
                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                }
                origin.close();
            }
            out.close();
        } catch (Exception e) {
            log.error("", e);
        }
    }

    public static byte[] files2zip(List<String> list_files) {
        ByteArrayOutputStream dest = null;
        try {
            BufferedInputStream origin = null;
            dest = new ByteArrayOutputStream();
            CheckedOutputStream checksum = new CheckedOutputStream(dest, new Adler32());
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(checksum));
            byte data[] = new byte[BUFFER];
            File fill = null;
            for (int i = 0; i < list_files.size(); i++) {

                fill = new File(list_files.get(i));

                FileInputStream fi = new FileInputStream(fill);
                origin = new BufferedInputStream(fi, BUFFER);

                ZipEntry entry = new ZipEntry(list_files.get(i));
                out.putNextEntry(entry);
                int count;
                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                }
                origin.close();
            }
            out.close();
        } catch (Exception e) {
            log.error("", e);
        }
        return dest.toByteArray();
    }
}