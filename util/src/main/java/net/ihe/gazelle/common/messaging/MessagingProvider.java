package net.ihe.gazelle.common.messaging;

public interface MessagingProvider {

	void receiveMessage(Object message);

}
