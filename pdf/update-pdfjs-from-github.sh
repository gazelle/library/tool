#!/usr/bin/env bash

rm -rf pdfjs
rm -rf src/main/resources/static/*
git clone git://github.com/mozilla/pdf.js.git pdfjs
cd pdfjs
sudo npm install -g gulp
npm install
node make generic
cp -Rv build/generic ../src/main/resources/static/
cd ../src/main/resources/static/
mv generic pdfjs
rm ./pdfjs/web/compressed.tracemonkey-pldi-09.pdf
cd ../../../../
rm -rf pdfjs
