package org.jboss.seam.pdf.ui;

import com.lowagie.text.DocWriter;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.html.HtmlWriter;
import com.lowagie.text.pdf.GazellePdfDocument;
import com.lowagie.text.pdf.GazellePdfWriter;
import com.lowagie.text.pdf.PdfDocument;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.rtf.RtfWriter2;

import javax.el.ValueExpression;
import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Date;

public class GazellePDFDocument extends UIComponentBase {

    public static DocumentType PDF = new DocumentType("pdf", "application/pdf");
    public static DocumentType RTF = new DocumentType("rtf", "text/rtf");
    public static DocumentType HTML = new DocumentType("html", "text/html");

    Document document;
    DocumentType documentType;


//	@Override
    //TODO SEE HOW IS THIS METHOD USED
	protected DocWriter createWriterForStream(OutputStream stream) throws DocumentException {

        if (documentType == PDF) {
			GazellePdfDocument.CREATION_DATE.set(getCreationDate());
			PdfDocument pdf = new GazellePdfDocument();
			document.addDocListener(pdf);
			PdfWriter writer = new GazellePdfWriter(pdf, stream);
			pdf.addWriter(writer);
			return writer;
		} else if (documentType == RTF) {
			return RtfWriter2.getInstance(document, stream);
		} else if (documentType == HTML) {
			return HtmlWriter.getInstance(document, stream);
		}

		throw new IllegalArgumentException("unknown document type");
	}

	protected Date getCreationDate() {
		return (Date) valueBinding(FacesContext.getCurrentInstance(), "creationDate", new Date());
	}

    public Object valueBinding(FacesContext context, String property, Object defaultValue) {
        Object value = defaultValue;
        ValueExpression expression = getValueExpression(property);
        if (expression != null) {
            value = expression.getValue(context.getELContext());
        }
        return value;
    }

    @Override
    public String getFamily() {
        return "net.ihe.gazelle.pdf";
    }

    static public class DocumentType implements Serializable {
        private String mimeType;
        private String extension;

        public DocumentType(String extension, String mimeType) {
            this.extension = extension;
            this.mimeType = mimeType;
        }

        public String getMimeType()
        {
            return mimeType;
        }

        public String getExtension()
        {
            return extension;
        }

    }
}
