package com.lowagie.text.pdf;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.ExceptionConverter;

public class GazellePdfWriter extends PdfWriter {

	private static Logger log = LoggerFactory.getLogger(GazellePdfWriter.class);

	public GazellePdfWriter() {
		super();
	}

	public GazellePdfWriter(PdfDocument document, OutputStream os) {
		super(document, os);
	}

	@Override
	public void close() {
		if (open) {
			if ((currentPageNumber - 1) != pageReferences.size()) {
				throw new RuntimeException("The page " + pageReferences.size()
						+ " was requested but the document has only " + (currentPageNumber - 1) + " pages.");
			}
			pdf.close();
			try {
				addSharedObjectsToBody();
				// add the root to the body
				PdfIndirectReference rootRef = root.writePageTree();
				// make the catalog-object and add it to the body
				PdfDictionary catalog = getCatalog(rootRef);
				// [C9] if there is XMP data to add: add it
				if (xmpMetadata != null) {
					PdfStream xmp = new PdfStream(xmpMetadata);
					xmp.put(PdfName.TYPE, PdfName.METADATA);
					xmp.put(PdfName.SUBTYPE, PdfName.XML);
					if ((crypto != null) && !crypto.isMetadataEncrypted()) {
						PdfArray ar = new PdfArray();
						ar.add(PdfName.CRYPT);
						xmp.put(PdfName.FILTER, ar);
					}
					catalog.put(PdfName.METADATA, body.add(xmp).getIndirectReference());
				}

				// [C11] Output Intents
				if (extraCatalog != null) {
					catalog.mergeDifferent(extraCatalog);
				}

				writeOutlines(catalog, false);

				// add the Catalog to the body
				PdfIndirectObject indirectCatalog = addToBody(catalog, false);
				// add the info-object to the body
				PdfIndirectObject infoObj = addToBody(getInfo(), false);

				// [F1] encryption
				PdfIndirectReference encryption = null;
				PdfObject fileID = null;

				try {
					Class<?> c = body.getClass();
					Method m = c.getDeclaredMethod("flushObjStm");
					m.setAccessible(true);
					m.invoke(body);
				} catch (Exception e) {
					log.error("ERROR", e);
				}

				// write the cross-reference table of the body
				body.writeCrossReferenceTable(os, indirectCatalog.getIndirectReference(),
						infoObj.getIndirectReference(), encryption, fileID, prevxref);

				// make the trailer
				// [F2] full compression
				if (fullCompression) {
					os.write(getISOBytes("startxref\n"));
					os.write(getISOBytes(String.valueOf(body.offset())));
					os.write(getISOBytes("\n%%EOF\n"));
				} else {
					PdfTrailer trailer = new PdfTrailer(body.size(), body.offset(),
							indirectCatalog.getIndirectReference(), infoObj.getIndirectReference(), encryption, fileID,
							prevxref);
					trailer.toPdf(this, os);
				}
				open = false;
				try {
					os.flush();
					if (closeStream) {
						os.close();
					}
				} catch (IOException ioe) {
					throw new ExceptionConverter(ioe);
				}
			} catch (IOException ioe) {
				throw new ExceptionConverter(ioe);
			}
		}
	}
}
