package com.lowagie.text.pdf;

import java.util.Calendar;
import java.util.Date;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;

public class GazellePdfDocument extends PdfDocument {

	public static final ThreadLocal<Date> CREATION_DATE = new ThreadLocal<Date>() {
		@Override
		protected Date initialValue() {
			return new Date();
		}
	};

	@Override
	public boolean add(Element element) throws DocumentException {
		if ((element != null) && (element.type() == Element.CREATIONDATE)) {
			if ((writer != null) && writer.isPaused()) {
				return false;
			}
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(CREATION_DATE.get());
			PdfString date = new PdfDate(calendar);
			info.put(PdfName.CREATIONDATE, date);
			info.put(PdfName.MODDATE, date);
			lastElementType = element.type();
			return true;
		} else {
			return super.add(element);
		}
	}
}
