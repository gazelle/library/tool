package net.ihe.gazelle.converter;

import net.ihe.gazelle.hql.criterion.IntervalType;
import net.ihe.gazelle.hql.criterion.date.DateValue;
import net.ihe.gazelle.dates.DateDisplayUtil;
import org.apache.commons.lang3.StringUtils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;
import java.io.Serializable;
import java.util.Date;

@FacesConverter(forClass = DateValue.class)
public class DateValueConvertor implements javax.faces.convert.Converter, Serializable {

	private static final long serialVersionUID = 739049097980035837L;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		DateValue result = new DateValue(DateDisplayUtil.getTimeZone());
		if (value != null) {
			String[] split = StringUtils.split(value, '|');

			result.setIntervalType(IntervalType.valueOf(split[0]));
			result.setValue1(new Date(Long.parseLong(split[1])));
			result.setValue2(new Date(Long.parseLong(split[2])));
		}
		return result;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if ((value != null) && (value instanceof DateValue)) {
			DateValue dateValue = (DateValue) value;
			return dateValue.getIntervalType().name() + "|" + dateValue.getValue1().getTime() + "|"
					+ dateValue.getValue2().getTime();
		}
		return null;
	}
}
