package net.ihe.gazelle.converter;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import java.io.Serializable;


@FacesConverter
@Deprecated
// TODO Is it really deprecated ? Previously defined for Seam (in s.taglib.xml under name converterEntity), currently not used (no usage of keyword found yet)
public class CustomEntityConverter implements Serializable, Converter {

	private static final long serialVersionUID = 3861619310986918190L;

	public static final String NULL_VALUE = "-1";

	public String getAsString(FacesContext facesContext, UIComponent cmp, Object value) throws ConverterException {
		if (value == null) {
			return null;
		}
		if (value instanceof String) {
			return (String) value;
		}
		try {
		    //TODO Implement Entity identifier store ?
			return "1";
		} catch (IllegalArgumentException e) {
			return NULL_VALUE;
		}
	}

	public Object getAsObject(FacesContext facesContext, UIComponent cmp, String value) throws ConverterException {
		if ((value == null) || (value.length() == 0)) {
			return null;
		}
		if (NULL_VALUE.equals(value)) {
			return null;
		}
        //TODO Implement Entity identifier store ?
		return value;
	}
}
