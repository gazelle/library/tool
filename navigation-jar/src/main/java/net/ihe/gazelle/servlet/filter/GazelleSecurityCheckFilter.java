package net.ihe.gazelle.servlet.filter;

import net.ihe.gazelle.cas.client.authentication.SSOIdentity;
import net.ihe.gazelle.exception.AuthorizationException;
import net.ihe.gazelle.exception.NotLoggedInException;
import net.ihe.gazelle.pages.Page;
import net.ihe.gazelle.pages.PageCheckAuthorization;
import net.ihe.gazelle.pages.PageLister;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.inject.Instance;
import javax.enterprise.inject.spi.CDI;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GazelleSecurityCheckFilter implements Filter{

    private static Logger log = LoggerFactory.getLogger(GazelleSecurityCheckFilter.class);

    public static ThreadLocal<String> LAST_SERVLET_PATH = new ThreadLocal<String>();

    private static Page[] PAGES = null;

    public static Page[] getPages() {
        if (PAGES == null) {
            synchronized (GazelleSecurityCheckFilter.class) {
                if (PAGES == null) {
                    List<Page> allPages = new ArrayList<Page>();
                    Instance<PageLister> pageListers = CDI.current().select(PageLister.class);
                    for (PageLister pageLister : pageListers) {
                        allPages.addAll(pageLister.getPages());
                    }
                    PAGES = allPages.toArray(new Page[allPages.size()]);
                }
            }
        }
        return PAGES;
    }

    public void storeRequestURL(HttpServletRequest request) {
        String requestURL = getServletURL(request);
        LAST_SERVLET_PATH.set(requestURL);
    }

    public static String getServletURL(HttpServletRequest request) {
        StringBuffer url = request.getRequestURL();
        String queryString = request.getQueryString();
        if (queryString != null) {
            url.append('?');
            url.append(queryString);
        }
        return url.toString();
    }

    protected void checkPermission(String servletPath) {

        String servletPathXHTML = servletPath.replace(".seam", ".xhtml");
        String servletPathSEAM = servletPath.replace(".xhtml", ".seam");

        boolean checked = false;
        boolean authorized = false;
        if (servletPathXHTML.startsWith("/a4j/") || servletPathXHTML.startsWith("/javax.faces.resource/")
                || ! servletPath.contains(".seam")) {
            checked = true;
            authorized = true;
        } else {
            Page[] pages = getPages();
            for (Page page : pages) {
                if (page.getLink().startsWith(servletPathXHTML) || page.getLink().startsWith(servletPathSEAM)) {
                    checked = true;
                    if (PageCheckAuthorization.isGranted(page)) {
                        authorized = true;
                    }
                }
            }
        }
        if (checked) {
            if (!authorized) {
                if (!CDI.current().select(SSOIdentity.class).get().isLoggedIn()){
                    throw new NotLoggedInException(" Access denied : " +
                            "Not logged in user has insufficient permission to access : " + servletPath);
                }
                String username = CDI.current().select(SSOIdentity.class).get().getUsername();
                if (username == null) {
                    username = "Null username";
                }
                log.warn(" Access denied. " + username
                        + " has insufficient permission to access : " + servletPath);
                throw new AuthorizationException(" Access denied. " + username
                        + " has insufficient permission to access : " + servletPath);
            }
        } else {
            log.error("No access rule defined for " + servletPath);
            // FIXME strict security mode
            throw new AuthorizationException("No access rule defined for this page");
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest originalRequest = (HttpServletRequest) request;
        String requestPath = originalRequest.getServletPath();
        storeRequestURL(originalRequest);
        checkPermission(requestPath);
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }
}
