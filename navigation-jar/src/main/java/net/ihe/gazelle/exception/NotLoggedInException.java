package net.ihe.gazelle.exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class NotLoggedInException extends RuntimeException {

    public NotLoggedInException(String message) {
        super(message);
    }
}
