package net.ihe.gazelle.exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class AuthorizationException extends RuntimeException {

    public AuthorizationException(String message) {
        super(message);
    }
}
