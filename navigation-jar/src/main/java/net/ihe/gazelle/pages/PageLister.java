package net.ihe.gazelle.pages;

import java.util.Collection;

public interface PageLister {

	Collection<Page> getPages();

}
