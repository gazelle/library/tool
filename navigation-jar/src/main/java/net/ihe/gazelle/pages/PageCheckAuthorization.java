package net.ihe.gazelle.pages;

public class PageCheckAuthorization {

	private PageCheckAuthorization() {
		super();
	}

	public static boolean isGranted(Page page, Object... context) {
		Authorization[] authorizations = page.getAuthorizations();
		for (Authorization authorization : authorizations) {
			if (!authorization.isGranted(context)) {
				return false;
			}
		}
		return true;
	}

}
