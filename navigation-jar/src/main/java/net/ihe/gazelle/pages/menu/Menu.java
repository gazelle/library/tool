package net.ihe.gazelle.pages.menu;

import java.util.List;

import net.ihe.gazelle.pages.Page;

public interface Menu {

	Page getPage();

	List<Menu> getItems();

	boolean isVisible();

}
