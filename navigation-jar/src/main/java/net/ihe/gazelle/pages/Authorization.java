package net.ihe.gazelle.pages;

public interface Authorization {

	boolean isGranted(Object... context);

}
