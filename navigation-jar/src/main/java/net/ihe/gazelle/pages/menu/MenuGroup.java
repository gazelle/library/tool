package net.ihe.gazelle.pages.menu;

import java.util.List;

import net.ihe.gazelle.pages.Page;

public class MenuGroup extends MenuEntry {

	private List<Menu> children;

	public MenuGroup(Page page, List<Menu> children) {
		super(page);
		this.children = children;
	}

	@Override
	public List<Menu> getItems() {
		return children;
	}

}
