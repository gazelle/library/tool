
package net.ihe.gazelle.converter;
import javax.ejb.Local;

@Local
public interface GenericConverterLocal {

	void registerConverters();

	void init();

	void stopGenericConverter();

}
