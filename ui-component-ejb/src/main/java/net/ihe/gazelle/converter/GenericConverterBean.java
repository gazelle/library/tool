package net.ihe.gazelle.converter;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.metadata.ClassMetadata;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.Map;


@Named("genericConverter")
@ApplicationScoped
public class GenericConverterBean implements GenericConverterLocal {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Initialized(ApplicationScoped.class)
    public void registerConverters() {

        Object delegate = entityManager.getDelegate();

        SessionFactoryImplementor factory = null;

        if (delegate instanceof Session) {
            Session session = (Session) delegate;
            SessionFactory sessionFactory = session.getSessionFactory();
            if (sessionFactory instanceof SessionFactoryImplementor) {
                factory = (SessionFactoryImplementor) sessionFactory;
            }
        }
        if (factory == null) {
            throw new IllegalArgumentException();
        }

        Map<String, ClassMetadata> allClassMetadata = factory.getAllClassMetadata();

        Collection<ClassMetadata> values = allClassMetadata.values();
        for (ClassMetadata classMetadata : values) {
            Class<?> mappedClass = classMetadata.getMappedClass();
            //TODO Check those lines
//            Init.instance().getConvertersByClass()
//                    .put(mappedClass, "net.ihe.gazelle.common.action.CustomEntityConverter");
        }
    }

    @Override
    @PostConstruct
    public void init() {
        // ?
    }

    @Override
    @PreDestroy
    public void stopGenericConverter() {
        // ?
    }
}
