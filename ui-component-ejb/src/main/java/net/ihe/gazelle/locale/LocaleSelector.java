package net.ihe.gazelle.locale;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.util.*;

@Named("localeSelector")
@SessionScoped
public class LocaleSelector implements Serializable {

    private String language;
    private String country;
    private String variant;
    private boolean cookieEnabled = true;
    private boolean localeChanged = false;

    @PostConstruct
    public void initLocale() {
        String localeString = getCookieValueIfEnabled();
        if (localeString!=null){
            setLocaleString(localeString);
        }
    }

    public String getLanguage() {
        return language;
    }

    /**
     * Get the selected locale
     */
    public Locale getLocale() {
        return calculateLocale(Locale.getDefault());
    }

    public boolean getLocalChanged(){
        return this.localeChanged;
    }

    public void homeReloadedWithLocalChanges(){
        this.localeChanged = false;
    }

    public void select() {
        FacesContext.getCurrentInstance().getViewRoot().setLocale(getLocale());
        ResourceBundle.clearCache();
        setCookieValueIfEnabled(getLocaleString());
    }

    public void select(ValueChangeEvent event) {
        setLocaleString( (String) event.getNewValue() );
        select();
    }

    public void selectLanguage(String language) {
        this.language = language;
        select();
    }

    public String getLocaleString(){
        return getLocale().toString();
    }

    public void setLocaleString(String localeString) {
        StringTokenizer tokens = new StringTokenizer(localeString, "-_");
        language = tokens.hasMoreTokens() ? tokens.nextToken() : null;
        country =  tokens.hasMoreTokens() ? tokens.nextToken() : null;
        variant =  tokens.hasMoreTokens() ? tokens.nextToken() : null;
        FacesContext.getCurrentInstance().getViewRoot().setLocale(calculateLocale(Locale.getDefault()));
        setCookieValueIfEnabled(getLocale().toString());

        this.localeChanged = true;
    }

    private String getCookieName() {
        return "net.ihe.gazelle.Locale";
    }

    private String getCookieValueIfEnabled() {
        return isCookieEnabled() ? getCookieValue() : null;
    }

    private String getCookieValue() {
        Cookie cookie = getCookie();
        return cookie==null ? null : cookie.getValue();
    }

    private Cookie getCookie() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        if (ctx != null) {
            return (Cookie) ctx.getExternalContext().getRequestCookieMap().get(getCookieName());
        } else {
            return null;
        }
    }

    private void setCookieValueIfEnabled(String value) {
        FacesContext ctx = FacesContext.getCurrentInstance();
        if (isCookieEnabled() && ctx != null) {
            HttpServletResponse response = (HttpServletResponse) ctx.getExternalContext().getResponse();
            Cookie cookie = new Cookie(getCookieName(), value);
            cookie.setMaxAge( 31536000 );
            cookie.setPath("/");
            response.addCookie(cookie);
        }
    }

    public boolean isCookieEnabled() {
        return cookieEnabled;
    }

    public void setCookieEnabled(boolean cookieEnabled) {
        this.cookieEnabled = cookieEnabled;
    }

    public List<SelectItem> getSupportedLocales() {
        List<SelectItem> selectItems = new ArrayList<SelectItem>();
        Iterator<Locale> locales = FacesContext.getCurrentInstance().getApplication().getSupportedLocales();
        while ( locales.hasNext() ) {
            Locale locale = locales.next();
            if ( !locale.getLanguage().isEmpty() ) {
                selectItems.add( new SelectItem( locale.toString(), locale.getDisplayName(locale) ) );
            }
        }
        return selectItems;
    }

    public Locale calculateLocale(Locale jsfLocale) {
        if (variant != null && !variant.isEmpty()) {
            return new java.util.Locale(language, country, variant);
        } else if (country != null && !country.isEmpty()) {
            return new java.util.Locale(language, country);
        } else if (language != null && !language.isEmpty()) {
            return new java.util.Locale(language);
        } else {
            return jsfLocale;
        }
    }
}
