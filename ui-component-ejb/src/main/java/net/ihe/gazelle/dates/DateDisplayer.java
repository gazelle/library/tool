package net.ihe.gazelle.dates;

import java.util.Date;

public interface DateDisplayer {

	Object displayDateTime(Date date);

}
