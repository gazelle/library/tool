package net.ihe.gazelle.dates;

import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

@Named("dateDisplay")
@ConversationScoped
public class DateDisplay implements Serializable {

    private static final long serialVersionUID = -4067186147576976512L;


    public DateDisplay() {
        super();
    }

    @Deprecated
    public static DateDisplay instance() {
        return null;
    }

    public String displayDate(Date date) {
        return DateDisplayUtil.displayDate(date);
    }

    public String displayDateTime(Date date, boolean useApplicationTimeZone) {
        return DateDisplayUtil.displayDateTime(date, useApplicationTimeZone);
    }

    public String displayDateTime(Date date) {
        return DateDisplayUtil.displayDateTime(date);
    }

    public String displayTime(Date date, boolean useApplicationTimeZone) {
        return DateDisplayUtil.displayTime(date, useApplicationTimeZone);
    }

    public String displayTime(Date date) {
        return DateDisplayUtil.displayTime(date);
    }

    public String displayDateTimeHTML(Date date) {
        return DateDisplayUtil.displayDateTimeHTML(date);
    }

    public String getCalendarPatternDateTime() {
        return DateDisplayUtil.getCalendarPatternDateTime();
    }

    public String getCalendarPatternDate() {
        return DateDisplayUtil.getCalendarPatternDate();
    }

    public Locale getCalendarLocale() {
        return DateDisplayUtil.getCalendarLocale();
    }

    public TimeZone getCalendarTimeZoneDateTime() {
        return DateDisplayUtil.getCalendarTimeZoneDateTime();
    }

    public TimeZone getCalendarTimeZoneDate() {
        return DateDisplayUtil.getCalendarTimeZoneDate();
    }
}
