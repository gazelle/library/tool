package net.ihe.gazelle.dates;

import java.util.Date;

public class DateDisplayerProvider implements DateDisplayer {

	@Override
	public Object displayDateTime(Date date) {
		return DateDisplayUtil.displayDateTime(date);
	}

}
