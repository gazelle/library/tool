package net.ihe.gazelle.dates;

import java.sql.Timestamp;

public class TimestampNano extends Timestamp {

    private static final long serialVersionUID = -7566216051090241297L;

    // number of millisecond, mircoseconds and nanoseconds in one second.
    protected static final long MILLI_MULTIPLIER = 1000L;
    protected static final long MICRO_MULIPLIER = MILLI_MULTIPLIER * 1000L;
    protected static final long NANO_MULTIPLIER = MICRO_MULIPLIER * 1000L;

    // number of seconds passed 1970/1/1 00:00:00 GMT.
    private static long sec0;
    // fraction of seconds passed 1970/1/1 00:00:00 GMT, offset by
    // the base System.nanoTime (nano0), in nanosecond unit.
    private static long nano0;

    static {
        // initialize base time in second and fraction of second (ns).
        long curTime = System.currentTimeMillis();
        sec0 = curTime / MILLI_MULTIPLIER;
        nano0 = (curTime % MILLI_MULTIPLIER) * MICRO_MULIPLIER - System.nanoTime();
    }

    public TimestampNano() {
        super(0);
        long nano_delta = nano0 + System.nanoTime();
        long sec1 = sec0 + (nano_delta / NANO_MULTIPLIER);
        long nano1 = nano_delta % NANO_MULTIPLIER;
        setTime(sec1 * MILLI_MULTIPLIER);
        setNanos((int) nano1);
    }
}
