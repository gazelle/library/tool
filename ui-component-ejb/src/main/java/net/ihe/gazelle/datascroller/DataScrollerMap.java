package net.ihe.gazelle.datascroller;

import java.util.HashMap;

public class DataScrollerMap extends HashMap<String, Integer> {

	private static final long serialVersionUID = -2619058410381373556L;

	@Override
	public Integer get(Object key) {
		Integer value = super.get(key);
		if (value == null) {
			return 1;
		}
		return value;
	}

}
