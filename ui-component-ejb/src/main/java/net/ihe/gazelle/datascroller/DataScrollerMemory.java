package net.ihe.gazelle.datascroller;

import net.ihe.gazelle.cas.client.authentication.SSOIdentity;
import net.ihe.gazelle.cookie.GazelleCookie;
import net.ihe.gazelle.preferences.PreferenceService;
import org.apache.commons.lang3.StringUtils;


import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Named("dataScrollerMemory")
@SessionScoped
public class DataScrollerMemory implements Serializable {

    private static final long serialVersionUID = 7973077771941839585L;
    private Integer numberOfResultsPerPage = null;

    @Inject
    private SSOIdentity identity;

    private static final List<Integer> POSSIBLE_NUMBER_OF_RESULTS_PER_PAGE = new ArrayList<Integer>();

    private static final String NUMBER_OF_ITEMS_PER_PAGE = "NUMBER_OF_ITEMS_PER_PAGE";
    private static final Integer DEFAULT_NUMBER_OF_RESULTS_PER_PAGE = 20;

    private static final int CONST_10_PAGES = 10;

    private static final int CONST_20_PAGES = 20;

    private static final int CONST_50_PAGES = 50;

    private static final int CONST_100_PAGES = 100;

    static {
        POSSIBLE_NUMBER_OF_RESULTS_PER_PAGE.add(CONST_10_PAGES);
        POSSIBLE_NUMBER_OF_RESULTS_PER_PAGE.add(CONST_20_PAGES);
        POSSIBLE_NUMBER_OF_RESULTS_PER_PAGE.add(CONST_50_PAGES);
        POSSIBLE_NUMBER_OF_RESULTS_PER_PAGE.add(CONST_100_PAGES);
    }

    private Map<String, Integer> scrollerPage = new DataScrollerMap();

    public Map<String, Integer> getScrollerPage() {
        return scrollerPage;
    }

    public List<Integer> getAllNumberOfResultsPerPage() {
        return POSSIBLE_NUMBER_OF_RESULTS_PER_PAGE;
    }

    public Integer getNumberOfResultsPerPage() {
        if (numberOfResultsPerPage != null) {
            return numberOfResultsPerPage;
        } else {
            if (!StringUtils.isEmpty(identity.getUsername())) {
                numberOfResultsPerPage = PreferenceService.getInteger(NUMBER_OF_ITEMS_PER_PAGE);
            } else {
                String cookie = GazelleCookie.getCookie(NUMBER_OF_ITEMS_PER_PAGE);
                if (cookie != null) {
                    try {
                        return Integer.valueOf(cookie);
                    } catch (NumberFormatException e) {
                        numberOfResultsPerPage = DEFAULT_NUMBER_OF_RESULTS_PER_PAGE;
                    }
                }
                numberOfResultsPerPage = DEFAULT_NUMBER_OF_RESULTS_PER_PAGE;
            }
            return numberOfResultsPerPage;
        }
    }

    public void setNumberOfResultsPerPage(Integer nb) {
        numberOfResultsPerPage = nb;
        if (!StringUtils.isEmpty(identity.getUsername())) {
            PreferenceService.setInteger(NUMBER_OF_ITEMS_PER_PAGE, numberOfResultsPerPage);
        } else {
            GazelleCookie.setCookie(NUMBER_OF_ITEMS_PER_PAGE, Integer.toString(numberOfResultsPerPage));
        }
    }
}
