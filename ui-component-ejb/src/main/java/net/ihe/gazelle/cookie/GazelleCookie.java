package net.ihe.gazelle.cookie;

import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GazelleCookie {

	private static final int MAX_AGE = 60 * 60 * 24 * 365 * 10;

	private GazelleCookie() {
		super();
	}

	public static void setCookie(String cookieId, String value) {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		setCookie(response, cookieId, value);
	}

	public static void setCookie(HttpServletResponse response, String cookieId, String value) {
		Cookie cookie = new Cookie(cookieId, value);
		cookie.setPath("/");
		cookie.setMaxAge(MAX_AGE);
		response.addCookie(cookie);
	}

	public static String getCookie(String cookieId) {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		return getCookie(request, cookieId);
	}

	public static String getCookie(HttpServletRequest request, String cookieId) {
		Cookie[] cookies = request.getCookies();
		String result = null;
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(cookieId)) {
					result = cookie.getValue();
				}
			}
		}
		return result;
	}
	
	public static void deleteCookie(HttpServletResponse response, String cookieId, String value, String cookiePath) {
		Cookie cookie = new Cookie(cookieId, value);
		cookie.setPath(cookiePath);
		cookie.setMaxAge(0);
		response.addCookie(cookie);
	}

}
