package net.ihe.gazelle.csv;

import java.util.List;

public interface CSVExportable {

	List<String> getCSVHeaders();

	List<String> getCSVValues();

}
