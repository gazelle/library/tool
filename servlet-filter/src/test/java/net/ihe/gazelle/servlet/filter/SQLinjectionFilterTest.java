package net.ihe.gazelle.servlet.filter;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class SQLinjectionFilterTest {

    @Test
    public void test_null_character_injections() {
        Assert.assertTrue(SQLinjectionFilter.isUnsafe("\0"));
        Assert.assertTrue(SQLinjectionFilter.isUnsafe("testing\0hello"));
    }

    @Test
    public void test_select_character_injections() {
        Assert.assertTrue(SQLinjectionFilter.isUnsafe("'select"));
        Assert.assertTrue(SQLinjectionFilter.isUnsafe("testing'select"));
    }

    @Test
    public void test_SQL_injections() {
        Assert.assertTrue(SQLinjectionFilter
                .isUnsafe("http://www.mydomain.com/products/products.asp?productid=123 'SELECT user-name, password FROM USERS"));
        Assert.assertTrue(SQLinjectionFilter
                .isUnsafe("http://www.mydomain.com/products/products.asp?productid=123; 'DROP TABLE Products"));
    }
}
