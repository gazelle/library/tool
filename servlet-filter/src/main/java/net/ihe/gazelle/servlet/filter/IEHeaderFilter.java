package net.ihe.gazelle.servlet.filter;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ApplicationScoped
@Named("net.ihe.gazelle.servlet.filter.IEHeaderFilter")
public class IEHeaderFilter implements Filter {

	private static final String HEADER_NAME1 = "X-UA-Compatible";
	private static final String HEADER_VALUE1 = "IE=EmulateIE8,chrome=1";
	private static final String HEADER_NAME2 = "Cache-Control";
	private static final String HEADER_VALUE2 = "no-cache";

	public static final ThreadLocal<HttpServletRequest> REQUEST = new ThreadLocal<HttpServletRequest>();

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
			ServletException {
		if (response instanceof HttpServletResponse) {
			HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			HttpServletResponse httpServletResponse = (HttpServletResponse) response;
			REQUEST.set(httpServletRequest);
			if (httpServletRequest.getRequestURI().endsWith(".seam")) {
				if (!httpServletResponse.containsHeader(HEADER_NAME1)) {
					httpServletResponse.setHeader(HEADER_NAME1, HEADER_VALUE1);
				}
				if (!httpServletResponse.containsHeader(HEADER_NAME2)) {
					httpServletResponse.setHeader(HEADER_NAME2, HEADER_VALUE2);
				}
			}
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {}

}
