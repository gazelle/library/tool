package net.ihe.gazelle.servlet.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface FileGenerator {

	String getPath();

	void process(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse);

}
