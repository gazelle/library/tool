package net.ihe.gazelle.servlet.filter;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ApplicationScoped
@Named("net.ihe.gazelle.servlet.filter.GeneratedFileFilter")
public class GeneratedFileFilter implements Filter {

	@Inject
	Instance<FileGenerator> fileGenerators;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
			ServletException {
		boolean handled = false;
		if (response instanceof HttpServletResponse) {
			final HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			final HttpServletResponse httpServletResponse = (HttpServletResponse) response;

			String servletPath = httpServletRequest.getServletPath();

			for (final FileGenerator fileGenerator : fileGenerators) {
				if (servletPath.startsWith(fileGenerator.getPath())) {
					fileGenerator.process(httpServletRequest, httpServletResponse);
					handled = true;
				}
			}

		}
		if (!handled) {
			chain.doFilter(request, response);
		}
	}

	@Override
	public void destroy() {}

}
